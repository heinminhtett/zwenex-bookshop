class ApplicationController < ActionController::Base
    before_action :authorize
  
    protected
      def authorize
        if session[:user_id]        
          # redirect_to books_url, notice: "Welcome"  
        else 
          
        end
      end
  
      def authorize_admin
        if !session[:is_admin]  
          redirect_to login_url, notice: 'Only superman can perform that kind of action.'
        end
      end
  end
  