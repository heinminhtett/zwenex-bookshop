class ReservationsController < ApplicationController
  skip_before_action :authorize, only: [:show, :index]
  #before_action :set_reservation, only: [:show, :destroy]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
    
  end

  # GET /reservations/new

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    book = Book.find(reservation_params["book_id"])
    book.update(is_reserved: true) 
    @user_id = session[:user_id]
    @reservation = Reservation.new(user_id: @user_id, book_id: book.id)

    respond_to do |format|
      if @reservation.save
        format.html { redirect_to books_url, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    book = Book.find(reservation_params["book_id"])
    book.update(is_reserved: false)
    @user_id = session[:user_id]
    @reservation = Reservation.find_by(book_id: book.id)

  
    respond_to do |format|
      if @reservation.delete
        format.html { redirect_to books_url, notice: 'Reservation was successfully destroyed.' }
        format.json { head :no_content }
      else

      end
        

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def reservation_params
       params.permit(:book_id)
    end
end
