class Api::RegisterationController < ApplicationController

  skip_before_action :verify_authenticity_token
  
  def register
    if params[:password] == params[:confirm_password]
      hashed_password = BCrypt::Password.create(params[:password])
      @user = User.new(name: params[:username], password_digest: hashed_password, is_admin: false)
      if @user.save
        render json: {
          user: @user,
          message: "Registeration successful."
          }, status: 200
      else
          render json: {
            message: "User is already existing in our database!!!"
          }, status: 422
      end
    else
        render json: {
          user: @user,
          message: "Password do not match"
          }, status: 422
    end
  end
end
