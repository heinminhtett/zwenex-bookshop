class Api::UsersController < ApplicationController
    def index
        @books = Book.all
        @user = User.find_by(id: params[:id])
        @reservations = Reservation.where(user_id: @user.id)
        # @book = Book.find_by(id: @reservation.book_id)
        array = Array.new
        @reservations.each do |reservation|
            @book = @books.find_by(id: reservation.book_id)
            array << {:id => @book.id,
                :name => @book.name,
                :published_year => @book.published_year,
                :is_reserved => @book.is_reserved,
                :categories => Category.where(id: @book.category_id)
            }
        end
        render json: {
            user: {
                name: @user.name
            },
            reserved_books: array
        }
        
        
    end
    
end
