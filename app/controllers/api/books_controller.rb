class Api::BooksController < ApplicationController
  
  def index
    array = Array.new
    @books = Book.all
    @categories = Category.all
    @books.each do |book|
      array << {:id => book.id,
        :name => book.name,
        :price => book.price,
        :published_year => book.publish_year,
        :is_reserved => book.is_reserved,
        :categories => Category.where(id: book.category_id)					
      }
    end
    render json: array
  end
  def show
    @books = Book.all
    @categories = Category.all
    @authors = Author.all
    @book = Book.find(params[:id])
    book_array = Array.new
    book_array << {:id => @book.id,
      :name => @book.name,
      :price =>@book.price,
      :published_year => @book.publish_year,
      :is_reserved => @book.is_reserved,
      :categories => Category.where(id: @book.category_id)
    }
    render json: book_array.first
  end
end
