class Api::UserauthController < ApplicationController
     
  skip_before_action :verify_authenticity_token

  def login
    user = User.find_by(name: params[:username])
    if user.try(:authenticate, params[:password])
      render json: {
        user: {
          id: user.id,
          name: user.name
        }
      }
    else
      render json: {
        message: "Username or Password were incorrect."
      },status: 401
    end
  end
end
