class Api::CategoriesController < ApplicationController
    
    def index
        array = Array.new
        @categories = Category.all
        render json: @categories 
    end
    
    def show
        
        @category = Category.find(params[:id])
        @books = Book.where(category_id: @category.id)
        booklist = Array.new
        @books.each do |book|
            booklist << {:id => book.id,
                :name => book.name,
                :published_year => book.publish_year,
                :is_reserved => book.is_reserved
            }
        end
        render json: {
            category: @category,
            books: booklist
        }
        
        
    end
    
end
