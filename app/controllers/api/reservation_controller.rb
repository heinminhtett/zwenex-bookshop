class Api::ReservationController < ApplicationController
    skip_before_action :verify_authenticity_token

    
  def show_reserved
    user_id = params[:user_id]
    book_id = params[:book_id]
    book = Book.find(book_id)
    if book.is_reserved
      
      reservation = Reservation.find_by(book_id: book.id)
      if reservation.user_id.to_s == user_id
        book.update(is_reserved: false)
        if reservation.delete
          render json: {
            message: "Reservation Cancelled.",
            book: book,
            reservation: reservation
          }                        
        else
          render json: {
            message: "Error Occured while cancelling reservation.",
            }, status: 401
          end
        else
          render json: {
            message: "Book is reserved",
            reservation_id: reservation.user_id,
            book_id: book_id,
            user_id: user_id,
            book: book
            }, status: 401
          end
        else
          book.update(is_reserved: true)
          reservation = Reservation.new(user_id: user_id, book_id: book.id)
          if reservation.save
            render json: {
              book_id: book_id,
              user_id: user_id,
              message: "You have been reserved Book",
              book: book,
              reservation: reservation
            }
          else 
            render json: {
              message: "Error Occured  while Reserving Book",
              book_id: book_id,
              user_id: user_id,
              book: book
              }, status: 401
            end
          
            
          end
          
        end
      
end
