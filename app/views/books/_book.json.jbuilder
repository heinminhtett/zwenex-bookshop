json.extract! book, :id, :name, :author, :publish_year, :price, :added_date, :created_at, :updated_at
json.url book_url(book, format: :json)
