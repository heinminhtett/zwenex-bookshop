class Book < ApplicationRecord
    belongs_to :category
    belongs_to :reservation, class_name: "reservation", foreign_key: "reservation_id", optional: true
    has_one_attached :header_image


    def self.search(search)
        if search.blank?
        all
        else
            where('name LIKE ?', "%#{search}%")
        end
        
    end
end
