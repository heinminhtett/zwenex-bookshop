Rails.application.routes.draw do
 
  controller :reservation do
    get 'create' => :create
    get 'delete' => :delete
    get 'show' => :show
    get 'index' => :index  
  end

  get 'reservations/create', to: 'reservations#create'
  get 'reservations/delete', to: 'reservations#destroy'
get 'reservations', to: 'reservations#index'
  get 'book/new', to: 'books#new'
  get 'edit' , to: 'books#edit'
  get 'admin' => 'admin#index'
  get 'register' => 'users#new'
  get 'categories/new', to: 'categories#new'
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
    get 'logout' => :destroy
  end


  resources :users
  resources :categories
  resources :books
  resources :reservations
  root 'books#index'


  namespace :api do
    
      #method
      get 'books' => 'books#index'
      get 'book' => 'books#show'
      
      get 'categories' => 'categories#index'
      get 'category' => 'categories#show'

      post 'login' => 'userauth#login'
      post 'register' => 'registeration#register'

      get 'user' => 'users#index'
      post 'reservation' => 'reservation#show_reserved'
    
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
