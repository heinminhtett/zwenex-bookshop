class AddIsReservedToBook < ActiveRecord::Migration[6.0]
  def change
    add_column :books, :is_reserved, :boolean, :default => false
  end
end
