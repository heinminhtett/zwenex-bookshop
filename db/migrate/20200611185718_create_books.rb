class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :name
      t.string :author
      t.string :publish_year
      t.integer :price
      t.date :added_date

      t.timestamps
    end
  end
end
